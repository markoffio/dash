//
//  ImageUploader.swift
//  Dash
//
//  Created by Marco Margarucci on 02/11/21.
//

import Firebase
import UIKit

/*
struct ImageUploader {
    // MARK: - Functions
    
    // Uplaod user image
    static func uploadImage(image: UIImage, completion: @escaping(String) -> ()) {
        // Compress image
        guard let imageData = image.jpegData(compressionQuality: 0.5) else { return }
        // Image filename
        let filename = NSUUID().uuidString
        // Firebase Storage reference
        let storageReference = Storage.storage().reference(withPath: "/profile_images/\(filename)")
        storageReference.putData(imageData, metadata: nil) { _, error in
            if let error = error {
                debugPrint("[ERROR]: Failed to upload image with error: \(error.localizedDescription)")
                return
            }
            storageReference.downloadURL { url, _ in
                guard let imageURL = url?.absoluteString else { return }
                completion(imageURL)
            }
        }
        
    }
}
*/
