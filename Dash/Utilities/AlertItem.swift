//
//  AlertItem.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton: Alert.Button
    
    var alert: Alert {
        Alert(title: title, message: message, dismissButton: dismissButton)
    }
}

struct AlertContext {
    // MARK: - Forgot password
    
    // Forgot password information
    static let forgotPasswordInformation = AlertItem(title: Text("Recupero password"), message: Text("Inserendo l'indirizzo email usato in fase di registrazione, riceverai le informazioni per recuperare la tua password."), dismissButton: .default(Text("OK")))
    
    // Reset password instructions sent via email
    static let passwordResetInstructionsSent = AlertItem(title: Text("Recupero password"), message: Text("Le istruzioni per recuperare la password, sono state inviate all'indirizzo email specificato."), dismissButton: .default(Text("OK")))
    
    // MARK: - Authentication
    
    // Registration successfully
    static let registrationSuccessfully = AlertItem(title: Text("Registrazione"), message: Text("Il tuo account è stato creato con successo."), dismissButton: .default(Text("OK")))
}
