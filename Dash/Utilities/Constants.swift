//
//  Constants.swift
//  Dash
//
//  Created by Marco Margarucci on 02/11/21.
//

import Firebase

// MARK: UI

// Week days
let days: [String] = ["D", "L", "M", "M", "G", "V", "S"]

// MARK: - Firebase

// Users collection
let COLLECTION_USERS = Firestore.firestore().collection("users")
// Customers collection
let COLLECTION_CUSTOMERS = Firestore.firestore().collection("customers")
// Reservations collections
let COLLECTION_RESERVATIONS = Firestore.firestore().collection("reservations")
