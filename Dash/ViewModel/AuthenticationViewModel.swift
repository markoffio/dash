//
//  AuthenticationViewModel.swift
//  Dash
//
//  Created by Marco Margarucci on 02/11/21.
//

import Firebase
import UIKit
import Combine
import SwiftUI

class AuthenticationViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    
    // Is registration view presented
    @Published var isRegistrationViewPresented: Bool = false
    // Is home view presented
    @Published var isHomeViewPresented: Bool = false
    // Is forgot password view presented
    @Published var isForgotPasswordViewPresented: Bool = false
    // Used to check if the user is authenticated
    @Published var didAuthenticatedUser: Bool = false
    // Temporary current user
    @Published var tempCurrentUser: FirebaseAuth.User?
    // User session
    @Published var userSession: FirebaseAuth.User?
    // Current user
    @Published var currentUser: User?
    // Alert item
    @Published var alertItem: AlertItem?
    // Is showing info alert
    @Published var isShowingInfoAlert: Bool = false
    // Shared instance
    static let shared = AuthenticationViewModel()
    
    override init() {
        super.init()
        // Initialize user session
        userSession = Auth.auth().currentUser
        // Fetch user
        fetchUser()
    }
        
    // MARK: - Functions
    
    // Show forgot password view
    func showPorgotPasswordView() {
        isForgotPasswordViewPresented.toggle()
    }
    
    // Show register view
    func showRegisterView() {
        isRegistrationViewPresented.toggle()
    }
    
    // Show home view
    func showHomeView() {
        isHomeViewPresented.toggle()
    }
    
    // Login user
    func login(withEmail email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { authDataResult, error in
            // Checking for errors
            if let error = error {
                self.handleError(error)
                return
            }
            guard let user = authDataResult?.user else { return }
            self.userSession = user
        }
    }
    
    // Register user
    func register(withEmail email: String, password: String, fullName: String) {
        Auth.auth().createUser(withEmail: email, password: password) { authDataResult, error in
            // Checking for errors
            if let error = error {
                self.handleError(error)
                return
            }
            guard let user = authDataResult?.user else { return }
            self.tempCurrentUser = user
            
            let data: [String: Any] = ["email": email, "fullName": fullName]
            COLLECTION_USERS.document(user.uid).setData(data) { _ in
                self.didAuthenticatedUser = true
                
            }
        }
        //self.alertItem = AlertContext.registrationSuccessfully
    }
    
    // Sign out
    func signOut() {
        self.userSession = nil
        do {
            try Auth.auth().signOut()
        } catch let error {
            self.handleError(error)
        }
    }
    
    // Send password reset
    func passwordReset(withEmail email: String) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            // Checking for errors
            if let error = error {
                self.handleError(error)
                return
            }
            self.alertItem = AlertContext.passwordResetInstructionsSent
        }
    }
    
    // Fetch user
    fileprivate func fetchUser() {
        // Get current user id
        guard let uid = userSession?.uid else { return }
        COLLECTION_USERS.document(uid).getDocument { snapshot, _ in
            guard let user = try? snapshot?.data(as: User.self) else { return }
            self.currentUser = user
        }
    }
    
    // Handle aunthetication error
    fileprivate func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            alertItem = AlertItem.init(title: Text("Errore"), message: Text(errorCode.errorMessage), dismissButton: .default(Text("OK")))
        }
    }
    
    // Show info alert
    func showInfoAlert() {
        isShowingInfoAlert.toggle()
        alertItem = AlertContext.forgotPasswordInformation
    }
}
