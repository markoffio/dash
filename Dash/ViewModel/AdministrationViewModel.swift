//
//  AdministrationViewModel.swift
//  Dash
//
//  Created by Marco Margarucci on 17/12/21.
//

import Foundation

final class AdministrationViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    // Used to check if the selected date is correct
    @Published var isCorrectDate: Bool = false
    // Alert item
    @Published var alertItem: AlertItem?
    // Used to show reservation detail view
    @Published var isShowingReservationDetailView: Bool = false
    // Used to update month on arrow click
    @Published var currentMonth: Int = 0
    // Selected date
    @Published var selectedDate: Date = Date()
    // Current date
    var currentDate: Date = Date()
    
    // Check if same day
    func isSameDay(date1: Date, date2: Date) -> Bool {
        let calendar = Calendar.current
        return calendar.isDate(date1, inSameDayAs: date2)
    }
    
    func isSelectedDay(date: Date) -> Bool {
        let calendar = Calendar.current
        return calendar.isDate(date, inSameDayAs: date)
    }
    
    func getDate() -> [String] {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM YYYY"
        let date = formatter.string(from: currentDate)
        return date.components(separatedBy: " ")
    }
    
    func extractSelectedDate() -> Date {
        let calendar = Calendar.current
        // Get current month
        guard let currentMonth = calendar.date(byAdding: .month, value: self.currentMonth, to: Date()) else { return Date() }
        return currentMonth
    }
    
    func getCurrentMonthName() -> String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func getSelectedMonthName(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "it")
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: date)
        return nameOfMonth
    }
    
    func getDayOfTheWeek(date: Date) -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        return dayInWeek
    }
    
    func checkIsClosed(date: Date) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        switch dayInWeek {
        case "Sunday", "Monday":
            return true
        default:
            return false
        }
    }
    
    // Extract date
    func extractDate() -> [DateValue] {
        let calendar = Calendar.current
        // Get current month
        let currentMonth = extractSelectedDate()
        var days = currentMonth.getAllDates().compactMap { date -> DateValue in
            // Get day
            let day = calendar.component(.day, from: date)
            let weekDay = calendar.component(.weekday, from: date)
            return DateValue(day: day, date: date, weekDay: weekDay)
        }
        let firstWeekDay = calendar.component(.weekday, from: days.first?.date ?? Date())
        for _ in 0..<firstWeekDay - 1 {
            days.insert(DateValue(day: -1, date: Date(), weekDay: 1), at: 0)
        }
        return days
    }


    // Extract date
    func getDays() -> [String] {
        var daysArray = [String]()
        let calendar = Calendar.current
        // Get current month
        let currentMonth = extractSelectedDate()
        var days = currentMonth.getAllDates().compactMap { date -> DateValue in
            // Get day
            let day = calendar.component(.day, from: date)
            let weekDay = calendar.component(.weekday, from: date)
            return DateValue(day: day, date: date, weekDay: weekDay)
        }
        let firstWeekDay = calendar.component(.weekday, from: days.first?.date ?? Date())
        for _ in 0..<firstWeekDay - 1 {
            days.insert(DateValue(day: -1, date: Date(), weekDay: 1), at: 0)
        }
        for index in 0..<days.count {
            daysArray.append("\(days[index].day)")
        }
        return daysArray
    }
    
    func getTimeFromString(timeString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: timeString)?.addingTimeInterval(3200)
        return date ?? Date()
    }
    
    func isTimeSlotValid(lastTimeSlot: String, newTimeSlot: String) -> Bool {
        let numericalLastTimeSlot = Int(lastTimeSlot.filter { $0 != ":" })!
        let numericalInputTimeSlot = Int(newTimeSlot.filter { $0 != ":" })!
        return numericalInputTimeSlot > numericalLastTimeSlot
    }
}

