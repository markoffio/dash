//
//  ForgotPasswordViewModel.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import Foundation

final class ForgotPasswordViewModel: ObservableObject {
    // MARK: - Properties
    
    // Email address
    @Published var email: String = ""
    // Is showing info alert
    @Published var isShowingInfoAlert: Bool = false
    // Alert item
    @Published var alertItem: AlertItem?
    
    // MARK: - Functions
    
    // Show info alert
    func showInfoAlert() {
        isShowingInfoAlert.toggle()
        alertItem = AlertContext.forgotPasswordInformation
    }
}
