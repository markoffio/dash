//
//  LoginViewModel.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import Foundation

final class LoginViewModel: ObservableObject {
    // MARK: - Properties
    
    // Email address
    @Published var email: String = ""
    // Password
    @Published var password: String = ""
    // Is registration view presented
    @Published var isRegistrationViewPresented: Bool = false
    // Is home view presented
    @Published var isHomeViewPresented: Bool = false
    // Is forgot password view presented
    @Published var isForgotPasswordViewPresented: Bool = false
    
    // MARK: - Functions
    
    // Show forgot password view
    func showPorgotPasswordView() {
        isForgotPasswordViewPresented.toggle()
    }
    
    // Show register view
    func showRegisterView() {
        isRegistrationViewPresented.toggle()
    }
    
    // Show home view
    func showHomeView() {
        isHomeViewPresented.toggle()
    }
}

