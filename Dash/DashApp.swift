//
//  DashApp.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import SwiftUI
import Firebase

@main
struct DashApp: App {
    
    init() {
        // Configure Firebase
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(AuthenticationViewModel.shared)
        }
    }
}
