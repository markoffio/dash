//
//  View+Extensions.swift
//  Dash
//
//  Created by Marco Margarucci on 24/12/21.
//

import Foundation
import SwiftUI

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}
