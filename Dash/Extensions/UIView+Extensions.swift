//
//  UIView+Extensions.swift
//  Dash
//
//  Created by Marco Margarucci on 17/12/21.
//

import Foundation
import UIKit

extension UIView {
    #if targetEnvironment(macCatalyst)
    @objc(_focusRingType)
    var focusRingType: UInt {
        return 1 //NSFocusRingTypeNone
    }
    #endif
}
