//
//  Colors+Extensions.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import Foundation
import SwiftUI

// Colors
extension Color {
    // Background
    static let background = Color("background")
    // Brand primary
    static let brandPrimary = Color("brandPrimary")
    // Brand secondary
    static let brandSecondary = Color("brandSecondary")
    // Light gray
    static let lightGray = Color("lightGray")
    // Red primary
    static let redPrimary = Color("redPrimary")
    // Bright green
    static let brightGreen = Color("brightGreen")
}
