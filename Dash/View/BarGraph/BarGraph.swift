//
//  BarGraph.swift
//  Dash
//
//  Created by Marco Margarucci on 24/12/21.
//

import SwiftUI

struct BarGraph: View {
    
    // Analytics array
    var analytics: [Analytics]
    
    @State private var textVisible = false
    
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                GraphView()
            }
        }
        .padding()
        .background(Color.lightGray.opacity(0.3))
        .cornerRadius(20)
    }
    
    @ViewBuilder
    func GraphView() -> some View {
        GeometryReader { proxy in
            ZStack {
                VStack(spacing: 0) {
                    ForEach(getGraphLines(), id: \.self) { line in
                        HStack(spacing: 8) {
                            Text("\(Int(line))")
                                .font(.custom("AvenirNext-DemiBold", size: 13))
                                .foregroundColor(Color.gray)
                            Rectangle()
                                .fill(Color.gray.opacity(0.2))
                                .frame(height: 1)
                        }
                        .frame(maxHeight: .infinity, alignment: .bottom)
                        .offset(y: -15)
                    }
                }
                HStack {
                    ForEach(analytics) { analytic in
                        VStack(spacing: 0) {
                            if textVisible {
                                HStack {
                                    Text("\(Int(analytic.reservations))")
                                        .font(.custom("AvenirNext-DemiBold", size: 13))
                                        .padding(.bottom)
                                }
                            }
                            VStack(spacing: 5) {
                                Rectangle()
                                    .fill(Color.brandPrimary)
                                    .cornerRadius(8, corners: [.topLeft, .topRight])
                                    .frame(width: 40)
                                    .shadow(radius: 5)
                                    .onHover { over in
                                        textVisible = over
                                    }
                            }
                            .frame(width: 8)
                            .frame(height: getBarHeight(point: analytic.reservations, size: proxy.size))
                            Text(analytic.weekDay)
                                .font(.custom("AvenirNext-DemiBold", size: 13))
                                .frame(height: 25, alignment: .bottom)
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
                        .padding(.leading, 30)
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
            }
        }
        .frame(height: 400)
    }
    
    // MARK: - Functions
    
    // Get max value
    func getMax() -> CGFloat {
        let max = analytics.max { first, second in return second.reservations > first.reservations }?.reservations ?? 0
        return max
    }
    
    // Get sample graph linex based on max value
    func getGraphLines() -> [CGFloat] {
        let max = getMax()
        var lines: [CGFloat] = []
        lines.append(max)
        
        for index in 1...4 {
            let progress = max / 4
            lines.append(max - (progress * CGFloat(index)))
        }
        return lines
    }
    
    // Get bar height
    func getBarHeight(point: CGFloat, size: CGSize) -> CGFloat {
        let max = getMax()
        let height = (point / max) * (size.height - 80)
        return height
    }
}

/*
struct BarGraph_Previews: PreviewProvider {
    static var previews: some View {
        BarGraph()
    }
}
*/
