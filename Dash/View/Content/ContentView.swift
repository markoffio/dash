//
//  ContentView.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Properties
    
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    var body: some View {
        Group {
            if viewModel.userSession != nil { MainView() }
            else { LoginView() }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
