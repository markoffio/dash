//
//  AdministrationView.swift
//  Dash
//
//  Created by Marco Margarucci on 17/12/21.
//

import SwiftUI

struct Day {
    let isOpen: Bool
    let timeSlots: [String]
}

struct AdministrationView: View {
    // Title
    var title: String = ""
    // Authentication view model
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel
    // View model
    @StateObject private var viewModel = AdministrationViewModel()
    // Open
    @State private var isOpen: Bool = true
    // Selected day
    @State private var selectedDay: Int = 0
    // Grid columns
    let columns = Array(repeating: GridItem(.flexible()), count: 7)
    
    // Default time slots
    @State private var defaultTimeSlots = ["09:30", "10:30", "11:45", "13:30", "15:50", "17:30"]
    
    @State private var selectedTimeSlots: [String] = [String]()
    
    @State private var isSelectedTimeSlot: Bool = false
    
    @State private var lastDefaultTimeSlot: Date = Date()
    
    var body: some View {
        VStack {
            // Header
            HStack {
                VStack(alignment: .leading, spacing: 2) {
                    Text(title)
                        .font(.custom("AvenirNext-DemiBold", size: 24))
                        .foregroundColor(Color.brandPrimary)
                    
                }
                Spacer()
                // Current user
                Text(authenticationViewModel.currentUser?.fullName ?? "Admin")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .foregroundColor(.gray)
            }
            Divider()
            HStack {
                //
                VStack(spacing: 10) {
                    // Month
                    HStack {
                        Text(viewModel.extractSelectedDate().month.uppercased() + " " + viewModel.extractSelectedDate().year.uppercased())
                            .font(.custom("AvenirNext-DemiBold", size: 18))
                        Spacer()
                        HStack {
                            // Backward button
                            Button {
                                debugPrint("backward")
                                viewModel.currentMonth -= 1
                            } label: {
                                Image(systemName: "chevron.backward")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 18, height: 18)
                                    .foregroundColor(Color.brandPrimary)
                            }
                            // Forward button
                            Button {
                                debugPrint("forward")
                                viewModel.currentMonth += 1
                            } label: {
                                Image(systemName: "chevron.forward")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 18, height: 18)
                                    .foregroundColor(Color.brandPrimary)
                            }
                        }
                    }
                    .padding(.horizontal, 40)
                    HStack(spacing: 8) {
                        ForEach(days, id: \.self) { day in
                            Text(day)
                                .font(.custom("AvenirNext-DemiBold", size: 16))
                                .frame(maxWidth: .infinity)
                                .foregroundColor(day == "L" || day == "D" ? Color.redPrimary : Color.brandPrimary)
                        }
                    }
                    .padding(.top, 20)
                    // Calendar
                    LazyVGrid(columns: columns, spacing: 15) {
                        ForEach(viewModel.extractDate()) { value in
                            Button {
                                viewModel.selectedDate = value.date
                                selectedDay = value.day
                            } label: {
                                CardView(value: value)
                            }
                            .overlay {
                                // Highlight selected day in the calendar
                                Capsule()
                                    .fill(value.day == selectedDay ? Color.brandPrimary.opacity(0.4) : .clear)
                                    .frame(width: 40, height: 35)
                            }
                            .disabled(1...2 ~= value.weekDay) // Disable current button if selected day is sunday or monday
                        }
                    }
                    Spacer()
                }
                .padding(.leading, -40)
                Spacer()
                    .frame(width: 20)
                // Selected day properties
                VStack(alignment: .leading, spacing: 10) {
                    Text("Giorno: \(viewModel.selectedDate.toString)")
                        .font(.custom("AvenirNext-DemiBold", size: 18))
                    Toggle("Aperti", isOn: $isOpen)
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .tint(Color.brandPrimary)
                        .padding(.bottom, 10)
                    // Hours
                    if isOpen {
                        VStack {
                            HStack {
                                Text("Orari")
                                    .font(.custom("AvenirNext-DemiBold", size: 15))
                                    .foregroundColor(.gray)
                                Spacer()
                            }
                            Divider()
                            ScrollView(.vertical, showsIndicators: false) {
                                ForEach(defaultTimeSlots, id: \.self) { timeSlot in
                                    HStack {
                                        Text(timeSlot)
                                            .font(.custom("AvenirNext-DemiBold", size: 15))
                                        Spacer()
                                        ZStack {
                                            if selectedTimeSlots.contains(timeSlot) {
                                                Image(systemName: "square.fill")
                                                    .resizable()
                                                    .scaledToFit()
                                                    .frame(width: 22, height: 22)
                                                    .foregroundColor(Color.brandPrimary)
                                            } else {
                                                Image(systemName: "square")
                                                    .resizable()
                                                    .scaledToFit()
                                                    .frame(width: 22, height: 22)
                                                    .foregroundColor(Color.brandPrimary)
                                            }
                                        }
                                    }
                                    .onTapGesture {
                                        if selectedTimeSlots.contains(timeSlot) {
                                            selectedTimeSlots = selectedTimeSlots.filter { $0 != timeSlot }
                                        } else {
                                            selectedTimeSlots.append(timeSlot)
                                        }
                                        debugPrint("Selected time slots: \(selectedTimeSlots)")
                                    }
                                }
                            }
                            Divider()
                                .padding(.top, 10)
                            HStack {
                                DatePicker("Aggiungi nuovo orario", selection: $lastDefaultTimeSlot, displayedComponents: .hourAndMinute)
                                    .font(.custom("AvenirNext-DemiBold", size: 15))
                                Spacer()
                                    .frame(width: 10)
                                Button {
                                    if viewModel.isTimeSlotValid(lastTimeSlot: defaultTimeSlots.last!, newTimeSlot: lastDefaultTimeSlot.hour) {
                                        defaultTimeSlots.append(lastDefaultTimeSlot.hour)
                                    } else {
                                        debugPrint("Devi inserire un orario successivo alle ore \(defaultTimeSlots.last!)")
                                    }
                                } label: {
                                    Image(systemName: "plus.square")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 22, height: 22)
                                }
                                .tint(Color.brandPrimary)
                                Spacer()
                            }
                            .padding([.top, .bottom], 10)
                        }
                    }
                    HStack {
                        Spacer()
                        Button {
                            debugPrint("IMPOSTAZIONI")
                            debugPrint("MESE SELEZIONATO: \(viewModel.getSelectedMonthName(date: viewModel.selectedDate))")
                            debugPrint("GIORNO SELEZIONATO: \(selectedDay)")
                            debugPrint(Day(isOpen: isOpen, timeSlots: selectedTimeSlots))
                            selectedTimeSlots.removeAll()
                        } label: {
                            Text("Salva impostazioni")
                                .padding()
                                .font(.custom("AvenirNext-DemiBold", size: 16))
                                .foregroundColor(Color(.white))
                        }
                        .buttonStyle(.borderedProminent)
                        .buttonBorderShape(.roundedRectangle(radius: 5.0))
                        .controlSize(.small)
                        .tint(Color.brandPrimary)
                        .padding(.top, 10)
                    }
                    Spacer()
                }
                
            }
            .onAppear(perform: {
                lastDefaultTimeSlot = viewModel.getTimeFromString(timeString: defaultTimeSlots.last!)
            })
            .padding(.top, 20)
            Spacer()
        }
    }
    
    @ViewBuilder
    func CardView(value: DateValue) -> some View {
        VStack(spacing: 2) {
            if value.day != -1 {
                Text("\(value.day)")
                    .font(.custom("AvenirNext-DemiBold", size: 16))
                    .fontWeight(.semibold)
                    .foregroundColor(((value.weekDay == 1 || value.weekDay == 2) ? .gray : (value.date.toString == Date.now.toString ? Color.brightGreen : .black)))
                    .frame(maxWidth: .infinity)
            }
        }
        .padding(.vertical, 5)
        .frame(height: 40, alignment: .top)
    }
}

struct AdministrationView_Previews: PreviewProvider {
    static var previews: some View {
        AdministrationView()
    }
}
