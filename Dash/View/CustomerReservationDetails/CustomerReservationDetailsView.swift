//
//  CustomerReservationDetailsView.swift
//  Dash
//
//  Created by Marco Margarucci on 18/12/21.
//

import SwiftUI

struct CustomerReservationDetailsView: View {
    // Customer full name
    var customerFullName: String = ""
    // Reservation day
    var reservationDay: String = ""
    // Reservation time
    var reservationTime: String = ""
    // Reservation price
    var reservationPrice: String = ""

    // Used to show/hide controls to send message in app to the customer
    @State private var isMessageInAppControlsVisible: Bool = false
    // Message
    @State private var message: String = "Inserisci il messaggio da inviare..."
    
    var body: some View {
        VStack(alignment: .leading) {
            // Header
            HStack {
                VStack(alignment: .leading, spacing: 2) {
                    Text(customerFullName)
                        .font(.custom("AvenirNext-DemiBold", size: 24))
                        .foregroundColor(Color.brandPrimary)
                    Text(reservationDay)
                        .font(.custom("AvenirNext-DemiBold", size: 18))
                        .foregroundColor(.gray)
                }
                Spacer()
                HStack {
                    Button {
                        debugPrint("email customer")
                    } label: {
                        Image(systemName: "envelope")
                            .foregroundColor(.white)
                            .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                            .background(Color.brandPrimary)
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                    }
                    .padding()
                    Button {
                        isMessageInAppControlsVisible.toggle()
                    } label: {
                        Image(systemName: "message")
                            .foregroundColor(.white)
                            .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                            .background(isMessageInAppControlsVisible ? Color.brandSecondary: Color.brandPrimary)
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                    }
                    .padding()
                    Button {
                        debugPrint("call")
                    } label: {
                        Image(systemName: "phone")
                            .foregroundColor(.white)
                            .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                            .background(Color.brandPrimary)
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                    }
                    .padding()
                }
                .frame(height: 65)
            }
            Divider()
                .padding(.bottom, 10)
            HStack(alignment: .top) {
                // Customer reservation details
                VStack(alignment: .leading, spacing: 10) {
                    Text("Cliente: \(customerFullName)")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                    Text("Data prenotazione: \(reservationDay)")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                    Text("Ora prenotazione: \(reservationTime)")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                    Text("Prezzo: \(reservationPrice)")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                    Text("List servizi")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                    Text("Inserire la lista dei servizi")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(.red)
                    /*ScrollView(showsIndicators: false) {
                    }*/
                }
                Spacer()
                    .frame(maxWidth: .infinity)
                // Contact customer via message in app
                if (isMessageInAppControlsVisible) {
                    VStack(alignment: .leading, spacing: 10) {
                        HStack {
                            Spacer()
                            TextEditor(text: $message)
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                                .lineSpacing(10.0)
                                .frame(minWidth: 0, maxWidth: 400, minHeight: 0, maxHeight: 250)
                                .border(Color.lightGray)
                                .cornerRadius(8)
                        }
                        HStack {
                            Spacer()
                            Button {
                                debugPrint("send message")
                            } label: {
                                Image(systemName: "paperplane")
                                    .foregroundColor(.white)
                                    .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                                    .background(Color.brandPrimary)
                                    .clipShape(RoundedRectangle(cornerRadius: 15))
                            }
                        }
                    }
                }
            }
            Spacer()
        }
        .padding(.horizontal)
        Spacer()
    }
}

/*
struct CustomerReservationDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerReservationDetailsView()
    }
}
*/
