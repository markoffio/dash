//
//  LeftMenuView.swift
//  Dash
//
//  Created by Marco Margarucci on 17/12/21.
//

import SwiftUI

struct LeftMenuView: View {
    
    // Tabs
    let tabs: [Tab]
    // Current tab
    @State var currentTab: String = "calendar"
    // Authentication view model
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel
    // Animation
    @Namespace var animation
    // Selected tab
    @Binding var selectedTab: Int
    
    var body: some View {
        // Left menu
        List {
            ForEach(Array(tabs.enumerated()), id: \.offset) { index, tab in
                Button {
                    withAnimation(.spring(response: 0.2, dampingFraction: 0.5, blendDuration: 0.2)) {
                        currentTab = tab.imageName
                        selectedTab = index
                    }
                } label: {
                    HStack {
                        Spacer()
                        Text(tab.title)
                            .font(.custom("AvenirNext-DemiBold", size: 15))
                            .foregroundColor(currentTab == tab.imageName ? Color.brandPrimary : .gray)
                        Image(systemName: tab.imageName)
                            .resizable()
                            .renderingMode(.template)
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(currentTab == tab.imageName ? Color.brandPrimary : .gray)
                            .frame(width: 22, height: 22)
                            .frame(width: 80, height: 60)
                            .overlay(
                                HStack {
                                    if currentTab == tab.imageName {
                                        Capsule()
                                            .fill(Color.brandPrimary)
                                            .matchedGeometryEffect(id: "TAB", in: animation)
                                            .frame(width: 2, height: 40)
                                            .offset(x: 2)
                                    }
                                }
                                , alignment: .trailing
                            )
                    }
                }
                //.contentShape(Rectangle())
            }
        }
        Spacer()
        // Sign out button
        Button {
            authenticationViewModel.signOut()
        } label: {
            HStack {
                Spacer()
                Text("Esci")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .foregroundColor(Color.redPrimary)
                Image(systemName: "xmark.circle")
                    .resizable()
                    .renderingMode(.template)
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(Color.redPrimary)
                    .frame(width: 22, height: 22)
                    .frame(width: 80, height: 60)
            }
        }
        .padding(.bottom, 40)
    }
}
