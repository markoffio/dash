//
//  LoginView.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import SwiftUI

struct LoginView: View {
    // MARK: - Properties
    
    // Email address
    @State private var email: String = ""
    // Password
    @State private var password: String = ""
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    // Onboarding data
    var onboardingData: [OnboardingItem] = [
        OnboardingItem(imageName: "salon", title: "Barber Shop", description: "Gestisci in modo comodo e facile le prenotazioni dei tuoi clienti"),
        OnboardingItem(imageName: "analytics", title: "Statistiche", description: "Controlla le statistiche del tuo negozio")
    ]
    
    var body: some View {
        HStack {
            VStack {
                //Spacer()
                TabView {
                    ForEach(0..<onboardingData.count) { index in
                        let currentElement = onboardingData[index]
                        OnboardingCard(onboardingItem: currentElement)
                    }
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
                .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
                .padding(.top, 160)
                .padding(.bottom, 40)
            }
            .frame(width: 500, height: 600)
            .background(Color.lightGray.opacity(0.2))
            //Divider()
            VStack {
                Text("Login")
                    .font(.custom("AvenirNext-DemiBold", size: 32))
                    .foregroundColor(Color.brandPrimary)
                Divider()
                    .frame(width: 150)
                    .padding(.bottom, 60)
                VStack(spacing: 50) {
                    // Email text field
                    CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                        .keyboardType(.default)
                        .textInputAutocapitalization(.none)
                    // Password text field
                    CustomTextField(text: $password, imageName: "key", placeholder: "password", isSecureField: true)
                    // Forgot password
                    HStack {
                        Spacer()
                        Button {
                            viewModel.showPorgotPasswordView()
                        } label: {
                            Text("password dimenticata?")
                                .font(.custom("AvenirNext-DemiBold", size: 13))
                                .foregroundColor(.gray)
                        }
                        .fullScreenCover(isPresented: $viewModel.isForgotPasswordViewPresented, content: ForgotPasswordView.init)
                    }
                    // Login button
                    Button {
                        viewModel.login(withEmail: email, password: password)
                    } label: {
                        Text("LOGIN")
                            .font(.custom("AvenirNext-DemiBold", size: 15))
                            .foregroundColor(.white)
                            .frame(width: 300, height: 30)
                            .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                            .background(Color.brandPrimary)
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                            .shadow(color: .gray, radius: 5, x: 0.0, y: 2.0)
                            
                    }
                    .fullScreenCover(isPresented: $viewModel.isHomeViewPresented, content: MainView.init)
                    .alert(item: $viewModel.alertItem, content: { $0.alert })
                    // Register
                    HStack {
                        Text("Non hai un account?")
                            .font(.custom("AvenirNext-DemiBold", size: 13))
                            .foregroundColor(Color.brandPrimary)
                        Button {
                            viewModel.showRegisterView()
                        } label: {
                            Text("Registrati.")
                                .font(.custom("AvenirNext-DemiBold", size: 13))
                                .foregroundColor(Color.redPrimary)
                        }
                        .fullScreenCover(isPresented: $viewModel.isRegistrationViewPresented, content: RegisterView.init)
                    }
                }
                .padding(.horizontal, 40)
            }
            .frame(width: 500, height: 600)
        }
        .frame(width: 1000, height: 600)
        .background(.white)
        .clipShape(RoundedRectangle(cornerRadius: 15))
        .shadow(color: Color.lightGray, radius: 10, x: 0.0, y: 2.0)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

// Onboarding card
fileprivate struct OnboardingCard: View {
    // Onboarding item
    let onboardingItem: OnboardingItem
    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .center) {
                Image(onboardingItem.imageName)
                    .resizable()
                    .frame(width: geometry.size.width / 3, height: geometry.size.height / 2.5)
                    .frame(maxWidth: .infinity)
                Text(onboardingItem.title)
                    .font(.custom("AvenirNext-DemiBold", size: 24))
                    .foregroundColor(.black)
                    .padding()
                Text(onboardingItem.description)
                    .font(.custom("AvenirNext-DemiBold", size: 16))
                    .multilineTextAlignment(.center)
                    .foregroundColor(.gray)
                    .padding(.horizontal, 25)
            }
        }
    }
}
