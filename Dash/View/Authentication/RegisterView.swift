//
//  RegisterView.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import SwiftUI

struct RegisterView: View {
    // MARK: - Properties
    
    // Email address
    @State private var email: String = ""
    // Password
    @State private var password: String = ""
    // Full name
    @State private var fullName: String = ""
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // View model
    @ObservedObject var viewModel = AuthenticationViewModel()
    
    var body: some View {
        VStack {
            HStack {
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "arrow.left.circle")
                    .renderingMode(.template)
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(Color.redPrimary)
                    .frame(width: 22, height: 22)
                    .frame(width: 80, height: 60)
                }
                Spacer()
            }
            .padding(.top, -15)
            Text("Crea nuovo account")
                .font(.custom("AvenirNext-DemiBold", size: 32))
                .foregroundColor(Color.brandPrimary)
            Divider()
                .frame(width: 150)
                .padding(.bottom, 60)
            VStack(spacing: 40) {
                // Full name text field
                CustomTextField(text: $fullName, imageName: "person", placeholder: "nome e cognome", isSecureField: false)
                    .keyboardType(.default)
                    .textInputAutocapitalization(.none)
                // Email text field
                CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                    .keyboardType(.default)
                    .textInputAutocapitalization(.none)
                // Password text field
                CustomTextField(text: $password, imageName: "key", placeholder: "password", isSecureField: true)
                // Register button
                Button {
                    viewModel.register(withEmail: email, password: password, fullName: fullName)
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Text("REGISTRATI")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(.white)
                        .frame(width: 300, height: 30)
                        .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                        .background(Color.brandPrimary)
                        .clipShape(RoundedRectangle(cornerRadius: 15))
                        .shadow(color: .gray, radius: 5, x: 0.0, y: 2.0)
                }
                .alert(item: $viewModel.alertItem, content: { $0.alert })
                // Already have an account
                HStack {
                    Text("Hai già un account?")
                        .font(.custom("AvenirNext-DemiBold", size: 13))
                        .foregroundColor(Color.brandPrimary)
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Text("Login.")
                            .font(.custom("AvenirNext-DemiBold", size: 13))
                            .foregroundColor(Color.redPrimary)
                    }
                }
            }
            .padding(.horizontal, 40)
        }
        .frame(width: 700, height: 600)
        .background(.white)
        .clipShape(RoundedRectangle(cornerRadius: 15))
        .shadow(color: Color.lightGray, radius: 10, x: 0.0, y: 2.0)
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
