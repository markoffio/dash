//
//  ForgotPasswordView.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import SwiftUI

struct ForgotPasswordView: View {
    // MARK: - Properties
    
    // Email address
    @State private var email: String = ""
    // View model
    @ObservedObject var viewModel = AuthenticationViewModel()
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack {
            HStack {
                // Dismiss button
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "arrow.left.circle")
                    .renderingMode(.template)
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(Color.redPrimary)
                    .frame(width: 22, height: 22)
                    .frame(width: 80, height: 60)
                }
                Spacer()
                // Info button
                Button {
                    viewModel.showInfoAlert()
                } label: {
                    Image(systemName: "info.circle")
                    .renderingMode(.template)
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.gray)
                    .frame(width: 22, height: 22)
                    .frame(width: 80, height: 60)
                }
            }
            .padding(.top, -20)
            Text("Recupero password")
                .font(.custom("AvenirNext-DemiBold", size: 32))
                .foregroundColor(Color.brandPrimary)
            Divider()
                .frame(width: 150)
                .padding(.bottom, 60)
            VStack(spacing: 50) {
                // Email text field
                CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                    .keyboardType(.default)
                    .textInputAutocapitalization(.none)
                // Login button
                Button {
                    viewModel.passwordReset(withEmail: email)
                } label: {
                    Text("RECUPERA")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(.white)
                        .frame(width: 300, height: 30)
                        .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                        .background(Color.brandPrimary)
                        .clipShape(RoundedRectangle(cornerRadius: 15))
                        .shadow(color: .gray, radius: 5, x: 0.0, y: 2.0)
                }
            }
            .padding(.horizontal, 40)
            .alert(item: $viewModel.alertItem, content: { $0.alert })
        }
        .frame(width: 700, height: 400)
        .background(.white)
        .clipShape(RoundedRectangle(cornerRadius: 15))
        .shadow(color: Color.lightGray, radius: 10, x: 0.0, y: 2.0)
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
