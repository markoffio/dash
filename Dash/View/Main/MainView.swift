//
//  MainView.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import SwiftUI
import UIKit

struct Tab: Hashable {
    let title: String
    let imageName: String
}

struct MainView: View {
    // MARK: - Properties
    
    // Selected tab
    @State var selectedTab: Int = 0
    // Tabs
    let tabs: [Tab] = [
        .init(title: "Prenotazioni", imageName: "calendar"),
        .init(title: "Statistiche", imageName: "chart.bar.xaxis"),
        .init(title: "Amministrazione", imageName: "hammer"),
        .init(title: "Utenti", imageName: "person.2"),
        .init(title: "Clienti", imageName: "heart"),
        .init(title: "Informazioni", imageName: "info.circle"),
        .init(title: "Impostazioni", imageName: "gearshape")
    ]

    var body: some View {
        NavigationView {
            LeftMenuView(tabs: tabs, selectedTab: $selectedTab)
                .padding(.top, -50)
                .frame(width: 316)
                .frame(minWidth: 300)
                .frame(maxHeight: .infinity, alignment: .top)
            switch selectedTab {
            case 0:
                ReservationsView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            case 1:
                AnalyticsView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            case 2:
                AdministrationView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            case 3:
                UsersView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            case 4:
                CustomersView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            case 5:
                InformationView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            case 6:
                SettingsView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            default:
                ReservationsView(title: tabs[selectedTab].title)
                    .padding(.top, -50)
                    .padding(.horizontal, 30)
            }
        }
        .accentColor(Color.brandPrimary)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



// MARK: - Extensions

extension UIScreen
{
#if os(watchOS)
    static let screenSize = WKInterfaceDevice.current().screenBounds.size
    static let screenWidth = screenSize.width
    static let screenHeight = screenSize.height
#elseif os(iOS) || os(tvOS)
    static let screenSize = UIScreen.main.nativeBounds.size
    static let screenWidth = screenSize.width
    static let screenHeight = screenSize.height
#elseif os(macOS)
    static let screenSize = NSScreen.main?.visibleFrame.size
    static let screenWidth = NSScreen.main?.visibleFrame.width //screenSize.width
    static let screenHeight = NSScreen.main?.visibleFrame.height //screenSize.height
#endif
    static let middleOfScreen = CGPoint(x: screenWidth / 2, y: screenHeight / 2)
}
