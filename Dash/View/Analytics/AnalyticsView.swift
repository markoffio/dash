//
//  AnalyticsView.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import SwiftUI

struct AnalyticsView: View {
    // Title
    var title: String = ""
    // Authentication view model
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel
    
    var analytics: [Analytics] = [
        Analytics(reservations: 10, weekDay: "Mar"),
        Analytics(reservations: 15, weekDay: "Mer"),
        Analytics(reservations: 5, weekDay: "Gio"),
        Analytics(reservations: 9, weekDay: "Ven"),
        Analytics(reservations: 12, weekDay: "Sab"),
    ]
    
    var body: some View {
        VStack {
            // Header
            HStack {
                VStack(alignment: .leading, spacing: 2) {
                    Text(title)
                        .font(.custom("AvenirNext-DemiBold", size: 24))
                        .foregroundColor(Color.brandPrimary)
                    
                }
                Spacer()
                // Current user
                Text(authenticationViewModel.currentUser?.fullName ?? "Admin")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .foregroundColor(.gray)
            }
            HStack {
                Spacer()
                // Search bar
                HStack(spacing: 10) {
                    Image(systemName: "magnifyingglass")
                        .font(.title3)
                        .foregroundColor(.black)
                    TextField("Cerca", text: .constant(""))
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(.gray)
                        .frame(width: 150)
                }
                .padding(.vertical, 8)
                .padding(.horizontal)
                .background(.white)
                .clipShape(Capsule())
                .shadow(color: Color.lightGray, radius: 5, x: 0.0, y: 2.0)
            }
            .padding(.top, 30)
            .padding(.bottom, 8)
            Divider()
            // MARK: - Body
            ScrollView(.vertical, showsIndicators: false) {
                // Prenotazioni settimanali
                VStack(spacing: 20) {
                    HStack {
                        Text("Prenotazioni settimanali: (inserire di quale settimana del mese si tratta)")
                            .font(.custom("AvenirNext-DemiBold", size: 18))
                        Spacer()
                        Button {
                            saveToJSONFile()
                        } label: {
                            Text("SALVA")
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                        }
                    }
                    BarGraph(analytics: analytics)
                        .padding(.horizontal)
                        .padding(.top, 20)
                }
                Spacer()
            }
            .padding(.top, 20)
        }
    }
    
    // MARK: - Test code
    
    /* ----------------------------------------------------------------------------------------------------------------------------------
     ____________                                                                                                            ____________
     ____________                                                TEST CODE                                                   ____________
     ____________                                                                                                            ____________
     ----------------------------------------------------------------------------------------------------------------------------------*/
    
    /*
    // Save analytics array to JSON file
    func saveToJsonFile() {
        // Get the url of Persons.json in document directory
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("analytics.json")

        let data = try! JSONEncoder().encode(analytics)
        var dataString: String { return String(data: data, encoding: .utf8)! }
        
        // Create a write-only stream
        guard let stream = TextOutputStream(toFileAtPath: fileUrl.path, append: false) else { return }
        stream.open()
        defer {
            stream.close()
        }

        debugPrint(dataString)
        
        dataString.write(to: &stream)
        
        // Transform array into data and save it into file
        var error: NSError?
        //JSONSerialization.writeJSONObject(dataString, to: stream, options: [], error: &error)

        // Handle error
        if let error = error {
            debugPrint(error)
        }
    }
    */
    
    func saveToJSONFile() {
        let data = try! JSONEncoder().encode(analytics)
        var dataString: String { return String(data: data, encoding: .utf8)! }
        let filename = getDocumentsDirectory().appendingPathComponent("analytics.json")

        do {
            try dataString.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        } catch let error {
            debugPrint(error)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

struct AnalyticsView_Previews: PreviewProvider {
    static var previews: some View {
        AnalyticsView()
    }
}
