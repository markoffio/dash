//
//  CustomersView.swift
//  Dash
//
//  Created by Marco Margarucci on 18/12/21.
//

import SwiftUI

struct CustomersView: View {
    // Title
    var title: String = ""
    // Authentication view model
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel
    
    var body: some View {
        VStack {
            // Header
            HStack {
                VStack(alignment: .leading, spacing: 2) {
                    Text(title)
                        .font(.custom("AvenirNext-DemiBold", size: 24))
                        .foregroundColor(Color.brandPrimary)
                    
                }
                Spacer()
                // Current user
                Text(authenticationViewModel.currentUser?.fullName ?? "Admin")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .foregroundColor(.gray)
            }
            HStack {
                Spacer()
                // Search bar
                HStack(spacing: 10) {
                    Image(systemName: "magnifyingglass")
                        .font(.title3)
                        .foregroundColor(.black)
                    TextField("Cerca", text: .constant(""))
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(.gray)
                        .frame(width: 150)
                }
                .padding(.vertical, 8)
                .padding(.horizontal)
                .background(.white)
                .clipShape(Capsule())
                .shadow(color: Color.lightGray, radius: 5, x: 0.0, y: 2.0)
            }
            .padding(.top, 30)
            .padding(.bottom, 8)
            Divider()
            Spacer()
        }
    }
}

struct CustomersView_Previews: PreviewProvider {
    static var previews: some View {
        CustomersView()
    }
}
