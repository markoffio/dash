//
//  MenuButton.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import SwiftUI

struct MenuButton: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct MenuButton_Previews: PreviewProvider {
    static var previews: some View {
        MenuButton()
    }
}
