//
//  ReservationsView.swift
//  Dash
//
//  Created by Marco Margarucci on 17/12/21.
//

import SwiftUI

struct ReservationsView: View {
    // Title
    var title: String = ""
    // Authentication view model
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel
    
    var body: some View {
        VStack {
            // Header
            HStack {
                VStack(alignment: .leading, spacing: 2) {
                    Text(title)
                        .font(.custom("AvenirNext-DemiBold", size: 24))
                        .foregroundColor(Color.brandPrimary)
                    Text("Giorno: \(Date().toString)")
                        .font(.custom("AvenirNext-DemiBold", size: 18))
                        .foregroundColor(.gray)
                    
                }
                Spacer()
                // Current user
                Text(authenticationViewModel.currentUser?.fullName ?? "Admin")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .foregroundColor(.gray)
            }
            HStack {
                Button {
                    debugPrint("Open datepicker")
                } label: {
                    Image(systemName: "calendar")
                        .foregroundColor(.white)
                        .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
                        .background(Color.brandPrimary)
                        .clipShape(RoundedRectangle(cornerRadius: 15))
                }

                Spacer()
                // Search bar
                HStack(spacing: 10) {
                    Image(systemName: "magnifyingglass")
                        .font(.title3)
                        .foregroundColor(.black)
                    TextField("Cerca", text: .constant(""))
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(.gray)
                        .frame(width: 150)
                }
                .padding(.vertical, 8)
                .padding(.horizontal)
                .background(.white)
                .clipShape(Capsule())
                .shadow(color: Color.lightGray, radius: 5, x: 0.0, y: 2.0)
            }
            .padding(.top, 30)
            .padding(.bottom, 8)
            Divider()
            HStack(spacing: 15) {
                Text("CLIENTE")
                    .font(.custom("AvenirNext-DemiBold", size: 18))
                    .foregroundColor(.black)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Text("GIORNO")
                    .font(.custom("AvenirNext-DemiBold", size: 18))
                    .foregroundColor(.black)
                    .frame(maxWidth: .infinity, alignment: .trailing)
                Text("ORA")
                    .font(.custom("AvenirNext-DemiBold", size: 18))
                    .foregroundColor(.black)
                    .frame(maxWidth: .infinity, alignment: .trailing)
                Text("PREZZO")
                    .font(.custom("AvenirNext-DemiBold", size: 18))
                    .foregroundColor(.black)
                    .frame(maxWidth: .infinity, alignment: .trailing)
            }
            .padding(.vertical, 20)
            .padding(.horizontal)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 16) {
                    ForEach(Array(zip(reservations.indices, reservations)), id: \.0) { index, reservation in
                        NavigationLink {
                            CustomerReservationDetailsView(customerFullName: reservation.customerFullName, reservationDay: reservation.date, reservationTime: reservation.time, reservationPrice: reservation.price)
                        } label: {
                            HStack(spacing: 15) {
                                Text(reservation.customerFullName)
                                    .font(.custom("AvenirNext-DemiBold", size: 15))
                                    .foregroundColor(.black)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                Spacer()
                                Text(reservation.date)
                                    .font(.custom("AvenirNext-DemiBold", size: 15))
                                    .foregroundColor(.gray)
                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                Text(reservation.time)
                                    .font(.custom("AvenirNext-DemiBold", size: 15))
                                    .foregroundColor(.gray)
                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                Text(reservation.price)
                                    .font(.custom("AvenirNext-DemiBold", size: 15))
                                    .foregroundColor(.gray)
                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                Image(systemName: "chevron.right")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 15, height: 15)
                                    .foregroundColor(Color.gray)
                                    .padding(.bottom, 3)
                            }
                            .padding(.vertical, 20)
                            .padding(.horizontal)
                            .background(index % 2 == 0 ? .white : Color.lightGray.opacity(0.3))
                            .cornerRadius(8)
                        }
                    }
                }
            }
        }
    }
}

struct ReservationsView_Previews: PreviewProvider {
    static var previews: some View {
        ReservationsView()
    }
}
