//
//  Analytics.swift
//  Dash
//
//  Created by Marco Margarucci on 24/12/21.
//

import Foundation

struct Analytics: Identifiable, Codable {
    var id = UUID().uuidString
    var reservations: CGFloat
    var weekDay: String
}

var analytics: [Analytics] = [
    Analytics(reservations: 10, weekDay: "Mar"),
    Analytics(reservations: 15, weekDay: "Mer"),
    Analytics(reservations: 5, weekDay: "Gio"),
    Analytics(reservations: 9, weekDay: "Ven"),
    Analytics(reservations: 12, weekDay: "Sab"),
]
