//
//  DateValue.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/10/21.
//

import SwiftUI

struct DateValue: Identifiable {
    var id = UUID().uuidString
    var day: Int
    var date: Date
    var weekDay: Int
}

extension DateValue {
    func toString() -> String {
        return String(self.day)
    }
}
