//
//  Reservation.swift
//  Dash
//
//  Created by Marco Margarucci on 31/10/21.
//

import SwiftUI

struct Reservation: Identifiable {
    var id = UUID().uuidString
    var customerFullName: String
    var date: String
    var time: String
    var price: String
}

var reservations: [Reservation] = [
    Reservation(customerFullName: "Gaetano Bombarzetti", date: Date().toString, time: "10:30" ,price: "34.00 €"),
    Reservation(customerFullName: "Giulio Rinaldi", date: Date().toString, time: "12:30" ,price: "15.00 €"),
    Reservation(customerFullName: "Saverio Piscopo", date: Date().toString, time: "16:00" ,price: "22.00 €"),
    Reservation(customerFullName: "Mariano Giroldi", date: Date().toString, time: "09:30" ,price: "10.00 €"),
    Reservation(customerFullName: "Amedeo Graziano", date: Date().toString, time: "11:00" ,price: "5.00 €"),
    Reservation(customerFullName: "Girolamo Baldi", date: Date().toString, time: "17:00" ,price: "25.00 €"),
]
