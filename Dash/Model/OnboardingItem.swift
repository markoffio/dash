//
//  OnboardingItem.swift
//  Dash
//
//  Created by Marco Margarucci on 01/11/21.
//

import Foundation

struct OnboardingItem {
    let imageName: String
    let title: String
    let description: String
}
