//
//  User.swift
//  Dash
//
//  Created by Marco Margarucci on 02/11/21.
//

import FirebaseFirestoreSwift

struct User: Identifiable, Decodable {
    // MARK: - Properties
    
    // User id
    @DocumentID var id: String?
    // Full name
    let fullName: String
    // Email
    let email: String
}
